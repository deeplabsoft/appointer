from django import template
from django.urls import reverse
register = template.Library()

@register.inclusion_tag('modal-form.html')
def create_modal_form(title, form, method, id):
    return {'title':title, 'form' : form, 'method':method, 'id':id}

@register.inclusion_tag('modal-confirm.html')
def create_modal_confirm(title, message, ajax):
    return {'title':title, 'message':message, 'ajax':ajax}