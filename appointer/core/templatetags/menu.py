from django import template
from django.urls import reverse
register = template.Library()

@register.inclusion_tag('menu.html')
def create_menu(path, is_authenticated):
    menus = [{'title':'About', 'link':reverse('core:about')}]
    if(not is_authenticated):
        menus.insert(0,{'title':'Login', 'link':reverse('core:login')})
    else:
        menus.insert(0,{'title':'Activities', 'link':reverse('activity:activities_list')})
        menus.append({'title':'Logout', 'link':reverse('core:logout')})
    return {'menus':menus, 'absolutepath' : path}