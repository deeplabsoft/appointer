from django.urls import path
from appointer.core import views
from django.contrib.auth import views as auth_views

app_name = 'core'
urlpatterns = [
    path('',views.home,name='home'),
    path('login/', auth_views.LoginView.as_view(template_name='login.html'), name='login'),
    path('logout/', auth_views.LogoutView.as_view(next_page='core:home'), name='logout'),
    path('about/', views.about, name='about'),
    path('password_reset/', views.reset_password, name='reset_password'),
    path('password_reset/<str:hash>', views.reset_password_confirm, name='reset_password_confirm')
]