from django import forms
from .models import PasswordReset
from .utils import password_reset_hash, send_mail
from django.contrib.auth import get_user_model

Usuario = get_user_model()

class PasswordResetForm(forms.Form):
    email = forms.EmailField(label='Email')

    def clean_email(self):
        email = self.cleaned_data.get('email')
        if Usuario.objects.filter(email=email).exists():
            return email
        else:
            raise forms.ValidationError('Nenhum usuario encontrado com o email {0}'.format(email))

    def save(self):
        user = Usuario.objects.get(email=self.cleaned_data.get('email'))
        res = PasswordReset(user=user, token=password_reset_hash(user.email))
        res.save()
        send_mail('BastielCos - Redefina sua senha','reset_password_mail.html', {'hash':res.token}, [user.email])

class PasswordResetConfirmForm(forms.Form):
    password1 = forms.CharField(label='Senha', widget=forms.PasswordInput)
    password2 = forms.CharField(label='Confirmação de senha', widget=forms.PasswordInput)

    def clean_password2(self):
        password1 = self.cleaned_data.get("password1")
        password2 = self.cleaned_data.get("password2")
        if password1 and password2 and password1 != password2:
            raise forms.ValidationError("Senhas divergentes")
        return password2

    def save(self, user=None, commit=True):
        user.set_password(self.cleaned_data["password1"])
        if commit:
            user.save()
        return user