from .models import Socials

def social(request):
    return {'social_media_links':Socials.objects.filter(is_main_site=False), 'main_site':Socials.objects.filter(is_main_site=True).first()}