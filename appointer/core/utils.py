import hashlib
import random
import string
from django.conf import settings
from django.template.loader import render_to_string
from django.utils.html import strip_tags
from django.core.mail import EmailMultiAlternatives

def send_mail(subject, template_name, context, recipient_list, from_email=settings.DEFAULT_FROM_EMAIL, fail_silently=False):
    if settings.DEBUG:
        context.update({'site':'http://localhost:8000'})
    else:
        pass
        #context.update({'site':'http://bastielcos.herokuapp.com'})
    html_content = render_to_string(template_name, context)
    text_content = strip_tags(html_content)
    msg = EmailMultiAlternatives(subject=subject, body=text_content, from_email=from_email, to=recipient_list)
    msg.attach_alternative(html_content, "text/html")
    msg.send(fail_silently=fail_silently)

def random_key(size=5):
    chars = string.ascii_uppercase + string.digits
    return ''.join(random.choice(chars) for x in range(size))

def password_reset_hash(salt, random_str_size=5):
    return hashlib.sha224(''.join(random_key(random_str_size)).join(salt).encode('utf-8')).hexdigest()