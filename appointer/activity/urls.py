from django.urls import path, re_path
from appointer.activity import views

app_name = 'core'
urlpatterns = [
    path('',views.activities_list,name='activities_list'),
    path('new', views.new_activity, name='new_activity'),
    re_path(r'(?P<pk>\d+)$', views.activity_details, name='activity_details'),
    re_path(r'(?P<pk>\d+)/delete$', views.delete_activity, name='delete_activity'),
    re_path(r'(?P<pk>\d+)/appoint$', views.appoint, name='new_appoint'),
    re_path(r'(?P<pk>\d+)/appoint/close$', views.close_appoint, name='close_appoint'),
    re_path(r'(?P<pk>\d+)/appoint/(?P<app_pk>\d+)/edit$', views.appoint_edit, name='edit_appoint'),
    re_path(r'(?P<pk>\d+)/break$', views.take_break, name='take_break'),
    re_path(r'(?P<pk>\d+)/break/resume$', views.resume_break, name='resume_break'),
    re_path(r'(?P<pk>\d+)/appoints/view$', views.list_appoints, name='view_appoints'),
]