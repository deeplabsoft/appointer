from django import template
from django.urls import reverse
register = template.Library()

@register.inclusion_tag('pagination.html')
def create_pagination(page_obj):
    return {'page_obj':page_obj}