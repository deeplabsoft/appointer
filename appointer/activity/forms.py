from django import forms
from django.forms.widgets import TextInput, DateTimeInput
from .models import Activity, Appoint

class ActivityForm(forms.ModelForm):
    class Meta:
        model = Activity
        fields = ['name','color']
        widgets = {
            'color': TextInput(attrs={'type': 'color'}),
        }

class AppointForm(forms.ModelForm):
    class Meta:
        model = Appoint
        fields = ['starttime', 'timeend']
        widgets = {
            # attrs={'type':"datetime-local"}
            'starttime': DateTimeInput(),
            'timeend': DateTimeInput()
        }

    timeend = forms.DateTimeField(required=False)