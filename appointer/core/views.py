from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.utils import timezone as tz
import calendar
from .forms import PasswordResetForm, PasswordResetConfirmForm
from .models import PasswordReset
import datetime
from django.contrib import messages

def home(req):
    count = 0
    act = []
    if req.user.is_authenticated:
        for activity in req.user.activities.all():
            weeks = []
            for week in calendar.monthcalendar(tz.now().year, tz.now().month):
                count = count + 1
                days = []
                for day in week:
                    if day == 0:
                        used = False
                        timespent = 0
                    else:
                        used = True
                        timespent = activity.total_appointed_time_day(day, tz.now().month, tz.now().year)
                    days.append({"day":day, "used":used, "timespentint":timespent})
                weeks.append(days)
            act.append({'name':activity.name, 'color':activity.color, 'appoints':weeks})
    return render(req, 'home.html', {'chartData':{"activities":act}})

def about(req):
    return render(req, 'about.html')

def reset_password(request):
    form = PasswordResetForm(request.POST or None)
    if form.is_valid():
        form.save()
        messages.success(request, 'Requisição enviada! Verifique seu email')
        return redirect('core:home')
        
    return render(request, 'password_reset.html', {'form':form})

def reset_password_confirm(request, hash):
    reset = PasswordReset.objects.filter(token=hash).first() # pylint: disable=no-member
    if reset == None:
        messages.error(request, 'Requisição não encontrada!')
        return redirect('core:home')

    if not reset.expired:
        if (((datetime.datetime.now() - reset.created_at).total_seconds())/60) > 30:
            reset.expired = True
            reset.save()
            messages.error(request, 'Requisição expirada! Tente novamente')
            return redirect('core:home')
        
        form = PasswordResetConfirmForm(request.POST or None)
        if form.is_valid():
            reset.confirmed = True
            reset.save()
            form.save(user=reset.user)
            messages.success(request, 'Senha alterada com sucesso!')
            return redirect('core:login')

        return render(request, 'password_reset_confirm.html', {'form':form})

    return redirect('core:home')