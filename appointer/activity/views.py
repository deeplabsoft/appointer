from django.shortcuts import render, redirect, get_object_or_404
from django.http import HttpResponse
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.contrib import messages
from django.contrib.humanize.templatetags.humanize import naturaltime
from django.views.generic import TemplateView, ListView, DetailView, View
from django.urls import reverse
from django.utils import timezone as tz
from django.core.paginator import Paginator
from .models import Activity, Appoint, Break
from .forms import ActivityForm, AppointForm
import json
import calendar

@login_required
def activities_list(req):
    paginator = Paginator(req.user.activities.all(), 10)
    page_obj = paginator.get_page(req.GET.get('page') or 1)
    return render(req, 'activities_index.html', {'form':ActivityForm(), 'page_obj':page_obj})

@login_required
def new_activity(req):
    form = ActivityForm(req.POST or None)
    if form.is_valid:
        act = form.save(commit=False)
        act.user = req.user
        act.save()
    if req.is_ajax():
        data = {
            'method':'NEW',
            'success': True, 
            'message': "New activity saved!", 
            'activity':{
                'num':req.user.activities.count(),
                'name':act.name,
                'id': str(act.id)
            }
        }
        return HttpResponse(json.dumps(data), content_type='appication/json')
    else:
        messages.success(req,"New activity saved!")
        return redirect('activity:activities_list')

@login_required
def delete_activity(req, pk):
    instnce = get_object_or_404(Activity, pk=pk)
    instnce.delete()
    if req.is_ajax():
        return HttpResponse(json.dumps({'success':True, 'count':req.user.activities.count()}), content_type="application/json")
    else:
        messages.success(req, "Activity {0} deleted!".format(instnce.name))
        return redirect('activity:activities_list')

@login_required
def appoint(request, pk):
    instance = get_object_or_404(Activity, pk=pk)
    if instance.get_open_appointments().count() > 0:
        if request.is_ajax():
            return HttpResponse(json.dumps({'msg':'open appointment for activity','success':False}), content_type='application/json')
        else:
            return redirect('activity:activity_details', pk=pk)
    app = Appoint(activity=instance)
    app.save()
    if request.is_ajax():
        data = {
            'total' : instance.appointments.count(),
            'success' : True,
            'endUrl': reverse('activity:close_appoint', args=[instance.pk]),
            'breakUrl': reverse('activity:take_break', args=[instance.pk]),
            'appstart': 'now'
        }
        return HttpResponse(json.dumps(data), content_type='application/json')
    else:
        return redirect('activity:activity_details', pk=pk)

@login_required
def close_appoint(request, pk):
    instance = get_object_or_404(Activity, pk=pk)
    if instance.get_open_appointments().count() < 1:
        if request.is_ajax():
            return HttpResponse(json.dumps({'msg':'no open appointment for activity','success':False}), content_type='application/json')
        else:
            return redirect('activity:activity_details', pk=pk)
    app = instance.get_open_appointments().first()
    app.timeend = tz.now()
    app.save()
    if app.has_open_break():
        b = app.breaks.filter(timeend__isnull=True).first()
        b.timeend = app.timeend
        b.save()
        
    if request.is_ajax():
        data = {
            'success':True
        }
        return HttpResponse(json.dumps(data), content_type='application/json')
    else:
        return redirect('activity:activity_details', pk=pk)

@method_decorator(login_required, name='dispatch')
class ActivityDetails(DetailView):
    model = Activity
    template_name = 'activity_details.html'
    context_object_name = 'activity'

    def get(self, request, *args, **kwargs):
        response = super(ActivityDetails, self).get(request, *args, **kwargs)
        return response

    def get_context_data(self, **kwargs):
        ctx = super(ActivityDetails, self).get_context_data(**kwargs)
        instance = get_object_or_404(Activity, id=self.kwargs['pk'])
        count = 0
        weeks = []
        for week in calendar.monthcalendar(tz.now().year, tz.now().month):
            count = count + 1
            days = []
            for day in week:
                if day == 0:
                    used = False
                    total = 0
                    timespent = 0
                    breaks = 0
                else:
                    used = True
                    total = instance.appointments.filter(starttime__date=tz.datetime(tz.now().year, tz.now().month, day).date()).count()
                    timespent = instance.total_appointed_time_day(day, tz.now().month, tz.now().year)
                    breaks = 0
                    for i in instance.appointments.filter(starttime__date=tz.datetime(tz.now().year, tz.now().month, day).date()):
                        breaks = breaks + i.breaks.all().count()
                days.append({"day":day, "used":used, "totalappointments":total, "timespent":str(tz.timedelta(seconds=timespent)).split('.', 2)[0], "breaks":breaks, "timespentint":timespent})
            weeks.append(days)
        ctx['monthlyApp'] = {"weeks":weeks,'color':instance.color}
        return ctx

    def post(self, request, *args, **kwargs):
        i = get_object_or_404(Activity, id=kwargs['pk'])
        form = ActivityForm(request.POST or None, instance=i)
        if form.is_valid:
            act = form.save()
        if request.is_ajax():
            return HttpResponse(json.dumps({'method':'SAVE', 'activity':{'name':act.name, 'id':act.id}}), content_type='appication/json')
        else:
            messages.success(request, "Activity {0} saved!".format(i.name))
            return redirect('activity:activities_list')

activity_details = ActivityDetails.as_view()

@login_required
def take_break(request, pk):
    instance = get_object_or_404(Activity, pk=pk)
    app = instance.get_open_appointments().first()
    if app.has_open_break():
        if request.is_ajax():
            return HttpResponse(json.dumps({'success':False, 'msg':'Activity has open break'}), content_type='application/json')
        else:
            return redirect('activity:activity_details', pk=pk)
    else:
        b = Break(appoint=app)
        b.save()

        if request.is_ajax():
            data = {
                'total' : app.breaks.count(),
                'success' : True,
                'breakstart': 'now',
                'resumeUrl': reverse('activity:resume_break', args=[pk])
            }
            return HttpResponse(json.dumps(data), content_type='application/json')
        else:
            return redirect('activity:activity_details', pk=pk)

@login_required
def resume_break(request, pk):
    instance = get_object_or_404(Activity, pk=pk)
    app = instance.get_open_appointments().first()
    if not app.has_open_break():
        if request.is_ajax():
            return HttpResponse(json.dumps({'success':False, 'msg':'no open breaks to resume'}), content_type='application/json')
        else:
            return redirect('activity:activity_details', pk=pk)

    b = app.breaks.filter(timeend__isnull=True).first()
    b.timeend = tz.now()
    b.save()
    if request.is_ajax():
        data = {
            'total' : app.breaks.count(),
            'success' : True,
            'takeUrl': reverse('activity:take_break', args=[pk])
        }
        return HttpResponse(json.dumps(data), content_type='application/json')
    else:
        return redirect('activity:activity_details', pk=pk)

@login_required
def list_appoints(request, pk):
    instance = get_object_or_404(Activity, pk=pk)
    paginator = Paginator(instance.appointments_month(tz.now().month, tz.now().year).all(), 10)
    page_obj = paginator.get_page(request.GET.get('page') or 1)
    return render(request, 'appoints_list.html', {'act':instance, 'page_obj':page_obj, 'form':AppointForm()})

@login_required
def appoint_edit(request, pk, app_pk):
    instance = get_object_or_404(Appoint, pk=app_pk)
    form = AppointForm(request.POST or None, instance=instance)
    if form.is_valid:
        act = form.save()
        s = True
    else:
        s = False
    if request.is_ajax():
        data = {'success':s}
        return HttpResponse(json.dumps(data), content_type='application/json')
    else:
        messages.success(request, "Appointment edited!")
        return redirect('activity:view_appoints', pk=pk)
