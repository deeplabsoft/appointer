from django.db import models
from django.conf import settings
from django.utils import timezone as tz

class Activity(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL, verbose_name='user', related_name='activities', on_delete=models.CASCADE)
    name = models.CharField('name', max_length=30, null=False, blank=False)
    color = models.CharField('color', max_length=7, null=False, blank=False)
    createdat = models.DateTimeField('created at', auto_now_add=True)

    def __str__(self):
        return self.name

    def get_open_appointments(self):
        return self.appointments.filter(timeend__isnull=True)

    def appointments_today(self):
        return self.appointments.filter(starttime__date=tz.now().date())

    def total_appointed_time_day(self, day, month, year):
        totalTime = 0

        for ap in self.appointments.filter(starttime__date=tz.datetime(year,month,day).date()):
            totalTime = totalTime + ap.totaltime()

        return totalTime

    def total_appointed_time(self):
        return str(tz.timedelta(seconds=self.total_appointed_time_day(tz.now().day, tz.now().month, tz.now().year))).split('.', 2)[0]

    def appointments_month(self, month, year):
        return self.appointments.filter(starttime__year=year, starttime__month=month)

    class Meta:
        verbose_name = 'Activity'
        verbose_name_plural = 'Activities'

class Appoint(models.Model):
    activity = models.ForeignKey(Activity, verbose_name='activity', related_name='appointments', on_delete=models.CASCADE)
    starttime = models.DateTimeField('start time', default=tz.now, null=False)
    timeend = models.DateTimeField('end time', null=True)
    
    def has_open_break(self):
        return self.breaks.filter(timeend__isnull=True).count() >= 1

    def totaltime(self):
        if not self.timeend is None:
            diff = (self.timeend - self.starttime).total_seconds()
        else:
            diff = (tz.now() - self.starttime).total_seconds()
        
        breaktime = 0 
        for b in self.breaks.all():
            if b.timeend is None:
                breaktime = breaktime + (tz.now() - b.starttime).total_seconds()
            else:
                breaktime = breaktime + (b.timeend - b.starttime).total_seconds()

        return (diff - breaktime)

    class Meta:
        verbose_name = 'Appointment'
        verbose_name_plural = 'Appointments'

class Break(models.Model):
    appoint = models.ForeignKey(Appoint, verbose_name='appointment', related_name='breaks', on_delete=models.CASCADE)
    starttime = models.DateTimeField('start time', auto_now_add=True)
    timeend = models.DateTimeField('end time', null=True)

    class Meta:
        verbose_name = 'Break'
        verbose_name_plural = 'Breaks'