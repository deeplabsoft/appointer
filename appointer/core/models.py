import re
from django.db import models
from django.core import validators
from django.contrib.auth.models import AbstractBaseUser, PermissionsMixin, UserManager

class User(AbstractBaseUser, PermissionsMixin):
    username = models.CharField('username', max_length=30, unique=True, validators=[validators.RegexValidator(regex=re.compile('^[\w.@+-]+$'), message='Invalid username', code='invalid')])
    email = models.EmailField('email', unique=True)
    name = models.CharField('name', max_length=100, blank=True)
    is_active = models.BooleanField('active', blank=True, default=True)
    is_staff = models.BooleanField('staff', blank=True, default=False)
    date_joined = models.DateTimeField('join date', auto_now_add=True)

    objects = UserManager()

    USERNAME_FIELD = 'username'
    REQUIRED_FIELDS = ['email']

    def __str__(self):
        return self.name or self.username

    def get_short_name(self):
        return self.username

    def get_full_name(self):
        return str(self)

    def get_open_activities(self):
        count = 0
        for act in self.activities.all():
            if act.get_open_appointments().count() > 0:
                count = count + 1

        return count 

    class Meta:
        verbose_name = 'User'
        verbose_name_plural = 'Users'

class Socials(models.Model):
    url = models.CharField('url', max_length=200, blank=False, null=False)
    icon = models.CharField('icon', max_length=50, blank=False, null=True)
    alt_text = models.CharField('alt_text', max_length=200, blank=False, null=False)
    is_main_site = models.BooleanField('is_main_site', null=False, default=False)

    def __str__(self):
        return self.alt_text

    class Meta:
        verbose_name = 'Social'
        verbose_name_plural = 'Socials'

class PasswordReset(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='password_resets')
    token = models.CharField('token', null=False, blank=False, max_length=255)
    created_at = models.DateTimeField('created_at', null=False, blank=False, auto_now_add=True)
    expired = models.BooleanField('expired', default=False, null=False, blank=False)
    confirmed = models.BooleanField('confirmed', default=False, blank=True)

    def __str__(self):
        return '{0} em {1}'.format(self.user, self.created_at)

    class  Meta:
        verbose_name = 'Reset de senha'
        verbose_name_plural = 'Resets de senha'
        ordering = ['-created_at']