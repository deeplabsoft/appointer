# Generated by Django 3.0.4 on 2020-11-30 07:59

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='PasswordReset',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('token', models.CharField(max_length=255, verbose_name='token')),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='created_at')),
                ('expired', models.BooleanField(default=False, verbose_name='expired')),
                ('confirmed', models.BooleanField(blank=True, default=False, verbose_name='confirmed')),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='password_resets', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'verbose_name': 'Reset de senha',
                'verbose_name_plural': 'Resets de senha',
                'ordering': ['-created_at'],
            },
        ),
    ]
